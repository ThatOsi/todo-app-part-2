import React, { useState } from "react";
import todosList from "./todos.json";
import TodoList from "./components/TodoList"
import { Link, Switch, Route } from "react-router-dom"
const App = ( ) => {
  const [todos, setTodos] = useState(todosList);
  const [newText, setNewText] = useState("");

 const addTodo = (e) => {
    if (e.keyCode === 13)  {
    const newTodos = todos;
    const newTodo = {
    userId: 1,
    id: Math.random(),
    title: newText,
    completed: false, 
      };

  newTodos.push(newTodo);
  setTodos(newTodos)
  setNewText("")
    }
  }

const toggleComplete = (todoId) => (e) => {
  const newTodos = todos.map((todo) =>
  todo.id === todoId ? {...todo,completed: !todo.completed } : { ...todo })
  setTodos(newTodos);
}
  
const handleDelete = (todoId) => (e) => {
  const newTodos = todos.map((todo) => todo.id !== todoId)
  setTodos(newTodos)
}

const clearCompleted = () => {
  const newTodos = todos.filter(todo => todo.completed === false)
setTodos(newTodos);
};

    return (
      <section className="todoapp">
        <header className="header">
          <h1>todos</h1>
          <input 
          className="new-todo" 
          name="newText"
          value={newText}
          onChange={(e) => setNewText(e.target.value)}
          onKeyDown={addTodo}
          placeholder="What needs to be done?" 
          autofocus 
          />
        </header>
        <Switch>
          <Route exact path="/" render= {(props) => (
           <TodoList{...props}
            todos={todos}
            toggleComplete={toggleComplete}
            handleDelete={handleDelete}
            />
          )}
          />
          <Route path="/active" render= {(props) => (
           <TodoList{...props}
            todos={todos.filter((todo) => !todo.completed)}
            toggleComplete={toggleComplete}
            handleDelete={handleDelete}
            />
          )}
          />
          <Route path="/completed" render= {(props) => (
           <TodoList{...props}
            todos={todos.filter((todo) => todo.completed)}
            toggleComplete={toggleComplete}
            handleDelete={handleDelete}
            />
          )}
          />
          </Switch>
          <footer className="footer">
          <span className="todo-count">
            <strong>{todos.filter(todo => !todo.completed).length}</strong> item(s) left
          </span>
          <ul className="filters">
            <li>
              <Link to="/"> All</Link>
            </li>
            <li>
              <Link to="/active">Active</Link>
            </li>
            <li>
              <Link to="/completed"> Completed</Link>
            </li>
          </ul>
          <button className="clear-completed" onClick={clearCompleted}>Clear completed</button>
        </footer>
      </section>
    );
  }



          export default App;
